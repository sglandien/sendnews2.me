function sendSubmitToDeleteProfile() {
    var e = document.getElementById("form-select");
    var profil = e.options[e.selectedIndex].text;
    var firstprofil = e.options[0].text;
    document.deleteProfile.profileToDelete.value = profil + ";" + firstprofil;
    document.deleteProfile.submit();
}

function addTableRow(){
    var tableRef = document.getElementById('schlagwortTable');
    var newRow = tableRef.insertRow(tableRef.rows.length);
    var newCell = newRow.insertCell(0);
    var newText = document.createTextNode(document.getElementById('schlagwortInput').value);
    if(document.getElementById('schlagwortInput').value.length == 0 || document.getElementById('schlagwortInput').value == null){ return false; }
    document.getElementById('schlagwortInput').value = "";
    document.getElementById('schlagwortInput').focus();
    newCell.appendChild(newText);
}