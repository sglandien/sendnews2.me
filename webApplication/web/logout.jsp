
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<%
    session.invalidate();
    response.sendRedirect("index.jsp");
%>
</html>
