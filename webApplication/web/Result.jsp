<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!-- Seite mit Ausgabe f�r erfolgreiches Mail-Senden -->


<html>
<head>
    <script src="scripts/navigationMenu.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="SendNews2.me Web Application">
    <meta name="keywords" content="web application, sendnews2me, SendNews2me, sendnews2.me, SendNews2.me">
    <meta name="author" content="Gerrit Christ, Felix Preuschoff">
    <title>Sende E-Mail</title>
    <link rel="stylesheet" href="./css/style.css">

    <style>
        /* Extra styles for the cancel button */
        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
        }

        /* The Overlay (background) */
        .overlay {
            /* Height & width depends on how you want to reveal the overlay (see JS below) */
            height: 0;
            width: 100%;
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            background-color: rgb(0,0,0); /* Black fallback color */
            background-color: rgba(0,0,0, 0.9); /* Black w/opacity */
            overflow-x: hidden; /* Disable horizontal scroll */
            transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
        }

        /* Position the content inside the overlay */
        .overlay-content {
            position: relative;
            top: 25%; /* 25% from the top */
            width: 100%; /* 100% width */
            text-align: center; /* Centered text/links */
            margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
        }

        /* The navigation links inside the overlay */
        .overlay a {
            padding: 8px;
            text-decoration: none;
            font-size: 36px;
            color: #818181;
            display: block; /* Display block instead of inline */
            transition: 0.3s; /* Transition effects on hover (color) */
        }

        /* When you mouse over the navigation links, change their color */
        .overlay a:hover, .overlay a:focus {
            color: #f1f1f1;
        }

        /* Position the close button (top right corner) */
        .overlay .closebtn {
            position: absolute;
            top: 20px;
            right: 45px;
            font-size: 60px;
        }

        /* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
        @media screen and (max-height: 450px) {
            .overlay a {font-size: 20px}
            .overlay .closebtn {
                font-size: 40px;
                top: 15px;
                right: 35px;
            }
        }
    </style>
</head>
<body>
<%

    if(request.getAttribute("Message") == null)
    {
        response.sendRedirect("index.jsp");
    }

    String name = null;
    try {
        name = (String) session.getAttribute("username");
    }catch(Exception e){
        name = null;
    }
%>
<header>

    <a style="font-size:40px;cursor:pointer; position: absolute; top:25px" onclick="openNav()">&#9776;</a>
    <a href="index.jsp"> <img class="center" src="res/Logo_white.png" alt="SendNews2.Me" height="50"></a>
    <button type="button" class="cancelbtn" style="width:auto;position:absolute;top:15px;right:20px;"><a href="logout.jsp">Logout</a></button>

</header>

<div id="myNav" class="overlay">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div class="overlay-content">
        <a href="index.jsp">Home</a>
        <a href="rss.jsp">RSS</a>
        <a href="EmailForm.jsp">E-Mail</a>
    </div>
</div>

<section>
    <span class="headertext"></span>
</section>

<h3><%=request.getAttribute("Message")%></h3>

</form>
</body>

<footer>
    <a target="_blank" href="https://sendnews2me.wixsite.com/sendnews2me/impressum"style="color: #FFFFFF">Impressum und Kontakt</a>
</footer>

</html>