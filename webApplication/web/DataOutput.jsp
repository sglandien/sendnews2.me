<%--
  Created by IntelliJ IDEA.
  User: svenja.glandien
  Date: 22.03.2018
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">



<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="SendNews2.me Web Application">
    <meta name="keywords" content="web application, sendnews2me, SendNews2me, sendnews2.me, SendNews2.me">
    <meta name="author" content="Gerrit Christ, Felix Preuschoff">
    <title>Impressum - SendNews2.me Web Application</title>
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
<header>
    <a href="index.jsp"><img src="res/Logo_white.png" alt="SendNews2.Me" height="50"></a>
    <div class="container">
        <nav>
            <li><a href="index.jsp">Home</a></li>
            <li><a href="rss.jsp">RSS</a></li>
            <li><a href="login.jsp">Login</a></li>
            <li><a class="current" href="impressum.jsp">Impressum</a></li>
            <li><a href="EmailForm.jsp">E-Mail</a></li>

        </nav>
    </div>
</header>
<h3><%=request.getAttribute("nachricht")%></h3>


</body>

<footer></footer>

</html>
