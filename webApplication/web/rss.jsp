<%--
  Created by IntelliJ IDEA.
  User: fpreuschoff
  Date: 15.03.2018
  Time: 12:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.io.FileReader" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="webapp.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <script src="scripts/changeRssContent.js"></script>
    <script src="scripts/navigationMenu.js"></script>
    <script src="scripts/popupBox.js"></script>
    <script src="scripts/deleteSelectedProfile.js"></script>
    <script src="scripts/saveTagsAndNewssites.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="SendNews2.me Web Application">
    <meta name="keywords" content="web application, sendnews2me, SendNews2me, sendnews2.me, SendNews2.me">
    <meta name="author" content="Gerrit Christ, Felix Preuschoff">
    <title>SendNews2.me Web Application | RSS</title>
    <link rel="stylesheet" href="./css/style.css">
    <style>
        button:hover {
            opacity: 0.8;
        }

        /* Extra styles for the cancel button */
        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
            cursor:pointer;
        }

        .deletebtn {
            width: 160px;
            padding: 12px;
            background-color: darkred;

            color: white;
            font-size: 13px;
            border: none;
        }


        .savebtn {
            width: 310px;
            padding: 10px 18px;
            background-color: green;
        }

        /* The Overlay (background) */
        .overlay {
            /* Height & width depends on how you want to reveal the overlay (see JS below) */
            height: 0;
            width: 100%;
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            background-color: rgb(0,0,0); /* Black fallback color */
            background-color: rgba(0,0,0, 0.9); /* Black w/opacity */
            overflow-x: hidden; /* Disable horizontal scroll */
            transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
        }

        /* Position the content inside the overlay */
        .overlay-content {
            position: relative;
            top: 25%; /* 25% from the top */
            width: 100%; /* 100% width */
            text-align: center; /* Centered text/links */
            margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
        }

        /* The navigation links inside the overlay */
        .overlay a {
            padding: 8px;
            text-decoration: none;
            font-size: 36px;
            color: #818181;
            display: block; /* Display block instead of inline */
            transition: 0.3s; /* Transition effects on hover (color) */
        }

        /* When you mouse over the navigation links, change their color */
        .overlay a:hover, .overlay a:focus {
            color: #f1f1f1;
        }

        /* Position the close button (top right corner) */
        .overlay .closebtn {
            position: absolute;
            top: 20px;
            right: 45px;
            font-size: 60px;
        }

        /* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
        @media screen and (max-height: 450px) {
            .overlay a {font-size: 20px}
            .overlay .closebtn {
                font-size: 40px;
                top: 15px;
                right: 35px;
            }
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
            .cancelbtn {
                width: 100%;
            }
        }

        table, td, th {
            border: 1px solid #ddd;
            text-align: left;
        }

        table {
            border-collapse: collapse;
            width: 310px;
        }

        th, td {
            padding: 15px;
        }
    </style>
</head>
<body>
<%
    String name = null;
    try {
        name = (String) session.getAttribute("username");
    }catch(Exception e){
        name = null;
    }
%>
<header>
    <% if(name == null){
        response.sendRedirect("index.jsp");
    }else{ %>
    <a style="font-size:40px;cursor:pointer; position: absolute; top:25px" onclick="openNav()">&#9776;</a>
    <a href="index.jsp"> <img class="center" src="res/Logo_white.png" alt="SendNews2.Me" height="50"></a>
    <button type="button" class="cancelbtn" onclick="logout()" style="cursor: pointer; font-size: 15px; color:white;width:auto;position:absolute;top:15px;right:20px;border:none;">LOGOUT</button>
    <script>
        function logout(){
            window.location.href = "/logout.jsp"
        }
    </script>
    <% }%>

</header>

<div id="myNav" class="overlay">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div class="overlay-content">
        <a href="index.jsp">Home</a>
        <a href="rss.jsp">Pressespiegel</a>
        <a href="EmailForm.jsp">E-Mail</a>
    </div>
</div>

<div id="container">
    <div id="left">
        <p style="color:red;">${message}</p>
        <p style="color:darkblue;">${successMessage}</p>

        <table>
            <tr>
                <form action="rss" method="post">
                    <button type="submit" name="submit" style="cursor: pointer;" value="refresh"><img src="res/refresh.png" width="50"></button>
                </form>
            </tr>
        </table>
        <br>
        <h2>Einstellungen:</h2>
        <table>
            <tr>
                <button class="createProfilebtn" style="cursor: pointer;" onclick="openPopupAndSend()">Neues Profil erstellen</button>
                <form name="submitNewProfile" action="createProfile" method="post">
                    <input type="hidden" name="newProfile" id=newProfile" value="">
                </form>

                <button type="button" class="deletebtn" style="cursor: pointer; color: white; border: 0;" onclick="sendSubmitToDeleteProfile()">Aktuelles Profil löschen</button>
                <form name="deleteProfile" action="deleteProfile" method="post">
                    <input type="hidden" name="profileToDelete" id=profileToDelete" value="">
                </form>
            </tr>
            <tr></tr>
        </table>

        <div class="custom-select" style="width:310px;">
            <select id="form-select">
                <%
                    UserDAO ud = new UserDAOImpl();
                    String activeProfile = ud.getActiveProfile(name);

                    Profilverwalten profilverwaltung = new Profilverwalten();
                    if(name!=null) {
                        String[] profile = profilverwaltung.getProfile(name);
                        if (profile == null) {
                            out.print("<option value=\"0\">Kein Profil vorhanden</option>");
                        } else {
                            for (int i = 0; i < profile.length; i++) {
                                if(profile[i].equals(activeProfile)){
                                    out.print("<option value=\"" + i + "\" selected>" + profile[i] + "</option>");
                                }else {
                                    out.print("<option value=\"" + i + "\">" + profile[i] + "</option>");
                                }
                            }
                        }
                    }
                %>
            </select>

            <script>
                var selectmenu = document.getElementById("form-select")
                selectmenu.onchange=function(){
                    var chosenoption = this.options[this.selectedIndex]
                    document.changeProfile.newActiveProfile.value = chosenoption.text;
                    document.changeProfile.submit();
                }
            </script>

            <form name="changeProfile" action="changeActiveProfile" method="post">
                <input type="hidden" name="newActiveProfile" id=newActiveProfile" value="">
            </form>
        </div>

        <script>
            var x, i, j, selElmnt, a, b, c;
            /*look for any elements with the class "custom-select":*/
            x = document.getElementsByClassName("custom-select");
            for (i = 0; i < x.length; i++) {
                selElmnt = x[i].getElementsByTagName("select")[0];
                /*for each element, create a new DIV that will act as the selected item:*/
                a = document.createElement("DIV");
                a.setAttribute("class", "select-selected");
                a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
                x[i].appendChild(a);
                /*for each element, create a new DIV that will contain the option list:*/
                b = document.createElement("DIV");
                b.setAttribute("class", "select-items select-hide");
                for (j = 0; j < selElmnt.length; j++) {
                    /*for each option in the original select element,
                    create a new DIV that will act as an option item:*/
                    c = document.createElement("DIV");
                    c.innerHTML = selElmnt.options[j].innerHTML;
                    c.addEventListener("click", function(e) {
                        /*when an item is clicked, update the original select box,
                        and the selected item:*/
                        var i, s, h;
                        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                        h = this.parentNode.previousSibling;
                        for (i = 0; i < s.length; i++) {
                            if (s.options[i].innerHTML == this.innerHTML) {
                                s.selectedIndex = i;
                                h.innerHTML = this.innerHTML;
                                var chosenoption = s.options[i]
                                document.changeProfile.newActiveProfile.value = chosenoption.text;
                                document.changeProfile.submit();
                                break;
                            }
                        }
                        h.click();
                    });
                    b.appendChild(c);
                }
                x[i].appendChild(b);
                a.addEventListener("click", function(e) {
                    /*when the select box is clicked, close any other select boxes,
                    and open/close the current select box:*/
                    e.stopPropagation();
                    closeAllSelect(this);
                    this.nextSibling.classList.toggle("select-hide");
                    this.classList.toggle("select-arrow-active");
                });
            }
            function closeAllSelect(elmnt) {
                /*a function that will close all select boxes in the document,
                except the current select box:*/
                var x, y, i, arrNo = [];
                x = document.getElementsByClassName("select-items");
                y = document.getElementsByClassName("select-selected");
                for (i = 0; i < y.length; i++) {
                    if (elmnt == y[i]) {
                        arrNo.push(i)
                    } else {
                        y[i].classList.remove("select-arrow-active");
                    }
                }
                for (i = 0; i < x.length; i++) {
                    if (arrNo.indexOf(i)) {
                        x[i].classList.add("select-hide");
                    }
                }
            }
            /*if the user clicks anywhere outside the select box,
            then close all select boxes:*/
            document.addEventListener("click", closeAllSelect);
        </script>

        <br></br>


        <h2>Profileinstellungen: </h2>

        <div class="dropdown">
            <button class="dropbtn">Wähle deine bevorzugten Nachrichtenseiten &#9661;</button>
            <div class="dropdown-content">
                <label for="one"><input type="checkbox" id="one"/>Tagesschau</label>
                <label for="two"><input type="checkbox" id="two"/>Stuttgarter Nachrichten</label>
            </div>
        </div>

        <div class="schlagwoerter">
            <table id="addSchlagwortTable" width="310">
                <tr>
                    <td width="310">
                        <input id="schlagwortInput" type="text" placeholder="Schlagwort hinzufügen" width="120px" height="40px" value="">
                        <button type="button" class="addTagbtn" style="cursor: pointer;" onclick="addTableRow()">Hinzufügen</button>
                    </td>
                </tr>
            </table>
            <table id="schlagwortTable" width="310">
                <%
                    if(activeProfile != null) {
                        profilverwaltung = new Profilverwalten();
                        String[][] result = profilverwaltung.getInformation(name, activeProfile);
                        if(result != null && result[0][0] != null && (result[0][1] != null || !result[0][0].equals(""))) {
                            for (int i = 0; i < result[0].length; i++) {
                                if(result[0][i] != null) {
                                    out.print("<tr><td>" + result[0][i] + "</td></tr>");
                                }
                            }
                        }
                    }
                %>
            </table>
        </div>

        <button id="saveTagsBtn" type="button" class="savebtn" style="cursor: pointer; color: white; border: none;" onclick="saveTagList()">Schlagwörter & Nachrichtenseiten speichern</button>
        <form name="saveTagsList" action="saveTags" method="post">
            <input type="hidden" name="tagsToSave" id="tagsToSave" value="">
        </form>

        <script>
            function saveTagList() {
                var table = document.getElementById("schlagwortTable");
                var themen = "";
                for (var r = 0, n = table.rows.length; r < n; r++) {
                    themen += table.rows[r].cells[0].innerHTML + ';';
                }
                document.saveTagsList.tagsToSave.value = themen;
                document.saveTagsList.submit();
            }
        </script>

    </div>

    <div id="right">
        <div class="wrapper" >
            <div class="tabs" >
                <!--<button onClick="openMe('pressespiegel')" class="tab">Pressespiegel</button>-->
                <!--<button onClick="openMe('sz')" class="tab">Stuttgarter Nachrichten</button>-->
            </div>

            <%if(session.getAttribute("username") != null){%>
            <div id="pressespiegel" class="content">
                <%
                    String jspPath = "/opt/archiv/";
                    String fileName = session.getAttribute("username") + ".txt";
                    String txtFilePath = jspPath + fileName;
                    StringBuilder sb = new StringBuilder();
                    sb.append("");
                    try {
                        BufferedReader reader = new BufferedReader(new FileReader(txtFilePath));
                        //BufferedReader br = new InputStreamReader(new FileInputStream(txtFilePath));
                        String line;
                        boolean feedsExisting = false;
                        while ((line = reader.readLine()) != null) {
                            feedsExisting = true;
                            String[] splittedLines = line.split("~%&\"");
                            sb.append("<div class=\"card\">\n");
                            sb.append("<h3>\n");
                            sb.append(splittedLines[0]);
                            sb.append("</h3>\n");
                            sb.append("<p> (" + splittedLines[5] + ")</p>" + "\n");
                            sb.append("<p>\n");
                            sb.append(splittedLines[1] + "\n");
                            sb.append("</p>\n");
                            sb.append("<a href=\"" + splittedLines[2] + "\">"+ "[Artikel lesen...]" +"</a>\n");
                            sb.append("</div>\n");
                        }
                        if(!feedsExisting){
                            out.print("<h4 style=\"margin-left: 200px;\">Keine Nachrichten im Archiv vorhanden.</h4>");
                        }
                    }catch(Exception e){
                        out.print("Exception reading from archive.");
                    }
                    out.println(sb.toString());
                %>
            </div>
            <%}%>
        </div>
    </div>
    <div id="clear"></div>
</div>

</body>
<footer id="footer" style="background-color: #35424a;">
    <a target="_blank" href="https://sendnews2me.wixsite.com/sendnews2me/impressum"style="color: #FFFFFF">Impressum und Kontakt</a>
</footer>
</html>
