package webapp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by fpreuschoff on 24.03.2018.
 */
@WebServlet(name = "createProfile")
public class createProfile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = (String)session.getAttribute("username");

        if(username == null){
            response.sendRedirect("index.jsp");
            return;
        }

        String profilname = request.getParameter("newProfile");

        Profilverwalten profilverwaltung = null;
        try{
            profilverwaltung = new Profilverwalten();
        }catch(FileNotFoundException fe){
            request.setAttribute("message","File nicht gefunden");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }catch(UnsupportedEncodingException ue){
            request.setAttribute("message","UnsupportedEncodingException");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }catch(IOException ioe){
            request.setAttribute("message","IOException");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }

        try {
            if (profilverwaltung.getInformation(username, profilname) != null) {
                request.setAttribute("message", "Profil existiert bereits.");
                request.getRequestDispatcher("rss.jsp").forward(request, response);
                return;
            }
        }catch(Exception e){
            request.setAttribute("message","Problem beim Abrufen der Informationen");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }

        try{
            String[] inputThemen = {};
            String[] inputMedien = {"ARD", "StuttgarterZeitung"};
            profilverwaltung.profilerstellenmitsave(username, profilname, inputThemen, inputMedien);
        }catch(Exception e){
            request.setAttribute("message","Profil konnte nicht erstellt werden.");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }

        UserDAO ud = new UserDAOImpl();
        if(ud.getActiveProfile(username).equals(" ")){
            try {
                ud.setActiveProfile(username, profilname);
            }catch(IOException e){
                request.setAttribute("message","IOException while getting/setting active profile.");
                request.getRequestDispatcher("rss.jsp").forward(request,response);
                return;
            }
        }

        request.setAttribute("successMessage","Neues Profil erstellt.");
        request.getRequestDispatcher("rss.jsp").forward(request,response);
    }
}
