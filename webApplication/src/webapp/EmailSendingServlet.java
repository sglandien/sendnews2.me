package webapp;

import java.io.IOException;

import javax.mail.Session;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * A servlet that takes message details from user and send it as a new e-mail
 * through an SMTP server.
 *
 * 
 */
@WebServlet("/EmailSendingServlet")
public class EmailSendingServlet extends HttpServlet {
	private String host;
	private String port;
	private String user;
	private String pass;

	public void init() {
		// reads SMTP server setting from web.xml file
		ServletContext context = getServletContext();
		host = context.getInitParameter("host");
		port = context.getInitParameter("port");
		user = context.getInitParameter("user");
		pass = context.getInitParameter("pass");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// reads form fields

		HttpSession userSession = request.getSession();
		String recipient = (String)userSession.getAttribute("useremail");

		// String subject = request.getParameter("subject");
		// String content = request.getParameter("content");

		// hier kann unser Text rein Subject --> Betreff und content --> Inhalt

		String subject = "Sportnachrichten 21.03.2018";
		String content = "Hier ist Ihr angeforderter Pressespiegel für das Profil Sportnachrichten.";


		String[] attachFiles = new String[1];
        attachFiles[0] = "/opt/archiv/Sportnachrichten_Thomas_21.03.18.pdf";



		String resultMessage = "";

		try {

			// �bergeben der einzelnen Variablen und Daten und Aufruf von sendEmail in EmailUtility.java

			//EmailUtility.sendEmail(host, port, user, pass, recipient, subject, content);
            EmailUtility.sendEmailWithAttachments(host, port, user, pass, recipient, subject, content, attachFiles);

			resultMessage = "The e-mail was sent successfully to " + recipient + ".";
		} catch (Exception ex) {

			// Bei Fehler hier ausgeben

			ex.printStackTrace();
			resultMessage = "There were an error: " + ex.getMessage();

		} finally {

			// Am Ende immer die Resultmessage auf der Seite Result.jsp ausgeben

			request.setAttribute("Message", resultMessage);
			getServletContext().getRequestDispatcher("/Result.jsp").forward( request, response);
		}
	}
}