package webapp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by fpreuschoff on 15.03.2018.
 */
@WebServlet(name = "rss")
public class rss extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("username") == null){
            response.sendRedirect("index.jsp");
        }


        PrintWriter out = response.getWriter();
        try {
            RssFeedParser tagesschauParser = new RssFeedParser("http://www.tagesschau.de/xml/rss2");
            RssFeedParser stuttgarterzeitungParser = new RssFeedParser("https://www.stuttgarter-nachrichten.de/rss/news.rss.feed");

            Feed tagesschauFeed = tagesschauParser.readFeed();
            Feed szFeed = stuttgarterzeitungParser.readFeed();

            Feed[] feeds = new Feed[2];
            feeds[0] = tagesschauFeed;
            feeds[1] = szFeed;

            Archivierer archiv = new Archivierer();
            if(!archiv.isExisting((String)request.getSession().getAttribute("username"))) {
                out.print("creating");
                archiv.createArchive((String)request.getSession().getAttribute("username"));
            }else{
                out.print("updating");
                try {
                    archiv.updateArchive((String)request.getSession().getAttribute("username"), feeds);
                }catch(Exception e){
                    out.print("Exception updating archive." + e.getMessage());
                }
            }

            /*if(!archiv.isExisting("sz")) {
                out.print("creating");
                archiv.createArchive("sz", szFeed);
            }else{
                out.print("updating");
                try {
                    archiv.updateArchive("sz", szFeed.getMessages().get(0).getPubDate(), szFeed);
                }catch(Exception e){
                    out.print("Exception updating archive." + e.getMessage());
                }
            }*/

        }catch(RuntimeException re) {
            out.print("Runtime Exception: Malformed Url or unable to parse RSS Feed.");
            return;
        }catch(IOException e){
            out.print("IOException.");
            return;
        }
        response.sendRedirect("rss.jsp");
    }
}
