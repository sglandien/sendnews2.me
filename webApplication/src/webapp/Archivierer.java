package webapp;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Archivierer {
    String path = "/opt/archiv/";
    Date lastArchiveDate = null;
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);


    public void updateArchive(String filename, Feed[] feeds) throws Exception {
        File file = new File(path + filename + ".txt");
        UserDAO ud = new UserDAOImpl();
        String activeProfile = ud.getActiveProfile(filename);

        for (Feed feed : feeds) {
            for (int p = 0; p < feed.getMessages().size(); p++) {
                Profilverwalten profilverwaltung = new Profilverwalten();
                String[][] result = profilverwaltung.getInformation(filename, activeProfile);
                boolean skip = true;
                for(int r=0; r<result[0].length; r++) {
                    if(feed.getMessages().get(p).getTitle().contains(result[0][r]) || feed.getMessages().get(p).getDescription().contains(result[0][r])) {
                        skip = false;
                    }
                }
                if(skip){
                    continue;
                }

                boolean found = false;
                FileInputStream fstream = new FileInputStream(file.getPath());
                BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

                String line = null;
                try {
                    line = br.readLine();
                }catch(Exception e){
                    line = null;
                }
                while (line != null) {
                    String[] parts = line.split("~%&\"");
                    if (parts[0].equals(feed.getMessages().get(p).getTitle())) {
                        found = true;
                        break;
                    }

                    line = br.readLine();
                    //parts = line.split("~%&\"");
                }
                fstream.close();
                br.close();

                if (!found) {
                    FileWriter fileWriter = new FileWriter(file.getPath(), true);
                    fileWriter.write(feed.getMessages().get(p).toString() + "\r\n");
                    fileWriter.close();
                }
            }
        }


        /*try {
            //DateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
            String tmp = br.readLine();
            lastArchiveDate = dateFormat.parse(tmp);
        } catch (IOException e) {
            throw new Exception();
        }

        if(lastArticleDate.compareTo(lastArchiveDate) <= 0){
            return;
        }else{
            updateDate(path + filename + ".txt", lastArticleDate);
            fstream.close();

            FileWriter fileWriter = new FileWriter(file.getPath(), true);
            for(int i = feed.getMessages().size()-1; i>-1; --i){
                if(feed.getMessages().get(i).getPubDate().after(lastArchiveDate)){
                    fileWriter.write(feed.getMessages().get(i).toString() + "\r\n");
                }
            }
            fileWriter.close();
        }*/
    }

    public void createArchive(String filename) throws IOException {
        File file = new File(path + filename + ".txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            throw new IOException(e);
        }

        /*FileWriter fileWriter = new FileWriter(file.getPath());
        fileWriter.write(dateFormat.format(feed.getMessages().get(0).getPubDate()) + "\r\n");
        for(int i = feed.getMessages().size()-1; i > -1; --i){
            fileWriter.write(feed.getMessages().get(i).toString() + "\r\n");
        }
        fileWriter.close();*/
    }

    public boolean isExisting(String filename){
        File f = new File(path + filename + ".txt");
        return f.exists();
    }

    /*private void updateDate(String path, Date newDate) throws Exception {
        File file = new File(path);
        String oldContent = "";
        BufferedReader reader = null;
        FileWriter writer = null;

        try{
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            String oldDate = line;
            while(line != null){
                oldContent = oldContent + line + System.lineSeparator();
                line = reader.readLine();
            }
            String newContent = oldContent.replaceAll(oldDate, dateFormat.format(newDate));
            writer = new FileWriter(file);
            writer.write(newContent);
        }catch(IOException e){
            throw new Exception(e);
        }finally {
            try{
                reader.close();
                writer.close();
            }catch(IOException e){
                throw new Exception(e);
            }
        }
    }*/
}
