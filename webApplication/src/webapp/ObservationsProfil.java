package webapp;

public class ObservationsProfil {
    String[] Themen;
    String[] Medien;
    String username;
    String profilname;


    public ObservationsProfil(String uname,String Pname,String[] Themen1, String[] Mediendienstleister1) {
        username = uname;
        profilname = Pname;
        Themen = Themen1;
        Medien  = Mediendienstleister1;
    }
    public String[] getThemen() {
        return Themen;
    }
    public String[] getMediendienstleister() {
        return Medien;
    }
    public String getUsername() {
        return username;
    }
    public String getProfilname() {
        return profilname;
    }

}