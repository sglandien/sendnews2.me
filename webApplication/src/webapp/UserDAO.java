package webapp;

/*
 ************** NOT USED ***********
 */

import java.io.IOException;

public interface UserDAO {

    String path = "/opt/userdatabase/";

    public int insertUser(User u) throws Exception;
    public User getUser(String username, String pass);
    public String getActiveProfile(String username);
    public void setActiveProfile(String username, String activeProfile) throws IOException;
}
