package webapp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by fpreuschoff on 24.03.2018.
 */
@WebServlet(name = "createProfile")
public class deleteProfile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = (String)session.getAttribute("username");

        if(username == null){
            response.sendRedirect("index.jsp");
            return;
        }

        String tmp = request.getParameter("profileToDelete");
        String profilname = tmp.split(";")[0];
        String newActiveProfile = tmp.split(";")[1];

        Profilverwalten profilverwaltung = null;
        try{
            profilverwaltung = new Profilverwalten();
        }catch(FileNotFoundException fe){
            request.setAttribute("message","File nicht gefunden");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }catch(UnsupportedEncodingException ue){
            request.setAttribute("message","UnsupportedEncodingException");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }catch(IOException ioe){
            request.setAttribute("message","IOException");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }

        try{
            if (profilverwaltung.deleteProfile(username,profilname)){
                UserDAO ud = new UserDAOImpl();
                ud.setActiveProfile(username, newActiveProfile);

                request.setAttribute("successMessage","Profil erfolgreich gelöscht.");
                request.getRequestDispatcher("rss.jsp").forward(request,response);
                return;
            }else{
                request.setAttribute("message","Profil nicht gefunden.");
                request.getRequestDispatcher("rss.jsp").forward(request,response);
                return;
            }
        }catch(Exception e){
            request.setAttribute("message","Fehler beim Löschen.");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }
    }
}
