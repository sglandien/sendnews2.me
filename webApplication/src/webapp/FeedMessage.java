package webapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FeedMessage {
    String title;
    String description;
    String link;
    String author;
    String guid;
    Date pubDate;

    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Date getPubDate(){return pubDate;}

    public void setPubDate(Date pubDate){this.pubDate = pubDate;}

    @Override
    public String toString() {
        return title + "~%&\"" + description
                + "~%&\"" + link + "~%&\"" + author + "~%&\"" + guid
                + "~%&\"" + dateFormat.format(pubDate) + "~%&\"";
    }
}
