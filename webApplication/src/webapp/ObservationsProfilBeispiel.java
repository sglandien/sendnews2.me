package webapp;

//BEISPIELDATEI NICHT NOTWENDIG

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class ObservationsProfilBeispiel {

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, IOException{
        Profilverwalten Profil = new Profilverwalten(); //WICHTIG ZUM INITIALISIEREN

      //Beispiel nach Profil suchen
        String[][] result;
        result = Profil.getInformation("bdiesch","russland");
        if(result != null)
        System.out.println(result[0][1]);

      //Beispiel Profil Löschen
        if (Profil.deleteProfile("Bernd","DHBW")){
            System.out.println("Profile deleted.");
        }else{
            System.out.println("Profil nicht Gefunden");
        }

      //Beispiel Profil hinzufügen
        String[] inputThemen = {"DHBW","DualeHochschule","Stuttgart"};
        String[] inputMedien = {"ARD","StuttgarterZeitung"};
        Profil.profilerstellenmitsave("Bernd","DHBW",inputThemen,inputMedien);

      //Beispiel Profil ändern (NUR THEMEN und MEDIEN ändern nicht den Profilname)
        String[] inputThemen2 = {"DHBW","DualeHochschule","Stuttgart"};
        String[] inputMedien2 = {"ARD","StuttgarterZeitung"};
        Profil.profilaendern("Bernd","DHBW",inputThemen2,inputMedien2);

        String[] profile = Profil.getProfile("felix");
        for(int k=0; k<profile.length;k++){
            System.out.println(k + ": " + profile[k]);
        }
        return;
    }
}
