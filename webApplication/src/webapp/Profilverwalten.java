package webapp;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.*;
import java.util.Scanner;

public class Profilverwalten{

    public static ObservationsProfil[] Profile;
    public static int AnzahlProfile = 0;

    //neues Profil erstellen (wird im Array Profile[] hinten angehängt)
    public static void profilerstellen(String username,
                                       String profilname,
                                       String[] Themen,
                                       String[] Mediendienstleister) throws FileNotFoundException, UnsupportedEncodingException {
        Profile[AnzahlProfile] = new ObservationsProfil(username,profilname,Themen,Mediendienstleister);
        AnzahlProfile ++;
        //savetofile();
        return;
    }

    //neues Profil erstellen und speichern (wird im Array Profile[] hinten angehängt)
    public static void profilerstellenmitsave(String username,
                                              String profilname,
                                              String[] Themen,
                                              String[] Mediendienstleister) throws FileNotFoundException, UnsupportedEncodingException {
        Profile[AnzahlProfile] = new ObservationsProfil(username,profilname,Themen,Mediendienstleister);
        AnzahlProfile ++;
        savetofile();
        return;
    }

    //Profil wird im Array (über username und Profilename) gesucht. Mediendienstleister und Themen werden geändert.
    public static int profilaendern(String username,
                                    String profilname,
                                    String[] Themen,
                                    String[] Mediendienstleister) throws FileNotFoundException, UnsupportedEncodingException {
        for (int i = 0; i < (AnzahlProfile); i++) { //alle Profile werden durchlaufen
            if (Profile[i].getUsername().equals(username) && Profile[i].getProfilname().equals(profilname)) { //falls Profil gefunden
                // System.out.println("Found");
                Profile[i] = new ObservationsProfil(username,profilname,Themen,Mediendienstleister);
                System.out.println("Profile found. Changed. ");
                savetofile();
                return 1;
            }
        }
        System.out.println("Profile NOT found no change");
        return 0;
    }


    static void savetofile() throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter("/opt/observationsprofile/observationsprofile.txt", "UTF-8");

        for (int i = 0; i < AnzahlProfile; i++) { // falls Profil gelöscht
            if(Profile[i].getUsername().equals("0"))
            {
                continue;
            }else{
                writer.println(Profile[i].getUsername());
            }
            writer.println(Profile[i].getProfilname());
            String[] Themen = Profile[i].getThemen();
            String[] Medien = Profile[i].getMediendienstleister();

            for (int k = 0; k < Themen.length; k++) {
                if(!Themen[k].equals("0"))
                    writer.print(Themen[k] + "~");
            }
            writer.println();
            for (int j = 0; j < Medien.length; j++) {
                if(!Medien[j].equals("0"))
                    writer.print(Medien[j] + "~");
            }
            writer.println();
        }
        writer.close();
        System.out.println("Objects Saved");
        return;
    }

    static void loadfromfile() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        AnzahlProfile = 0;
        String[] input = new String[400];
        int Zeilen = 0;

        FileInputStream fstream = new FileInputStream(new File("/opt/observationsprofile/observationsprofile.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        //Scanner scanner = new Scanner(new File("/opt/Obersationsprofile/observationsprofile.txt"));
        int i = 0;
        try {
            input[i] = br.readLine();
            while (input[i] != null) {
                i++;
                Zeilen++;
                input[i] = br.readLine();
            }
        }catch(IOException e){
            throw new IOException(e);
        }

        for (int k = 0; k < Zeilen; k += 4) {
            String[] Themen = input[k + 2].split("~");
            String[] Mediendienstleister = input[k + 3].split("~");

            profilerstellen(input[k], input[k + 1],
                    Themen,
                    Mediendienstleister);

        }
        //System.out.println(AnzahlProfile + " Objects Loaded");
        return;
    }


    public static boolean deleteProfile(String username,String profilname) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        for (int i = 0; i < (AnzahlProfile); i++) {
            if (Profile[i].getUsername().equals(username) && Profile[i].getProfilname().equals(profilname)) {
                Profile[i].username = "0";
                savetofile();
                loadfromfile();
                return true;
            }
        }
        return false;
    }

    public static String[] getProfile(String user){
        String[] result;
        String[] tmp = new String[25];
        int profilesfound = 0;
        for (int i = 0; i < (AnzahlProfile); i++) {
            if (Profile[i].getUsername().equals(user)) {
                if(profilesfound < 25){
                tmp[profilesfound] = Profile[i].getProfilname();
                profilesfound++;
                }
            }
        }
        if (profilesfound == 0){
            return null;
        }
        result = new String[profilesfound];

        for (int j = 0; j < profilesfound; j++) {
            result[j] = tmp[j];
        }
        return result;
    }

    public static String[][] getInformation(String username,String profilname){
        for (int i = 0; i < (AnzahlProfile); i++) {
            if (Profile[i].getUsername().equals(username) && Profile[i].getProfilname().equals(profilname)) {
                System.out.println("Profile found");
                int themenlaenge = Profile[i].getThemen().length;
                int mediendienstleisterlaenge = Profile[i].getMediendienstleister().length;
                int strlng;
                if (themenlaenge >= mediendienstleisterlaenge) {
                    strlng = themenlaenge;
                } else {
                    strlng = mediendienstleisterlaenge;
                }
                String[][] GefundenesProfil = new String[2][strlng];

                for (int j = 0; j < themenlaenge; j++) {
                    GefundenesProfil[0][j] = Profile[i].getThemen()[j];
                }
                for (int k = 0; k < mediendienstleisterlaenge; k++) {
                    GefundenesProfil[1][k] = Profile[i].getMediendienstleister()[k];
                }
                return GefundenesProfil;
            }
        }
        System.out.println("Profile not found");
        return null;
    }


    public Profilverwalten() throws FileNotFoundException, UnsupportedEncodingException, IOException{
        //String[][] amk;

        Profile = new ObservationsProfil[100];

        loadfromfile();
        return;
    }
}
