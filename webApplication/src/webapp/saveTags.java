package webapp;

import org.w3c.dom.UserDataHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by fpreuschoff on 25.03.2018.
 */
@WebServlet(name = "saveTags")
public class saveTags extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = (String)session.getAttribute("username");

        if(username == null){
            response.sendRedirect("index.jsp");
            return;
        }

        String param = request.getParameter("tagsToSave");
        String[] themen = param.split(";");

        Profilverwalten profilverwaltung = null;
        try{
            profilverwaltung = new Profilverwalten();
        }catch(FileNotFoundException fe){
            request.setAttribute("message","File nicht gefunden");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }catch(UnsupportedEncodingException ue){
            request.setAttribute("message","UnsupportedEncodingException");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }catch(IOException ioe){
            request.setAttribute("message","IOException");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }

        UserDAO ud = new UserDAOImpl();
        String activeProfile = null;
        try {
            activeProfile = ud.getActiveProfile(username);
        }catch(Exception e){
            request.setAttribute("message","Exception getting active profile.");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }
        try {
            profilverwaltung.profilaendern(username, activeProfile, themen, new String[]{"ARD", "StuttgarterZeitung"});
        }catch(Exception e){
            request.setAttribute("message","Exception while changing profile data.");
            request.getRequestDispatcher("rss.jsp").forward(request,response);
            return;
        }

        request.setAttribute("successMessage","Schlagwörter und Nachrichtenseiten aktualisiert.");
        request.getRequestDispatcher("rss.jsp").forward(request,response);
    }
}
