package webapp;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class UserDAOImpl implements UserDAO{


    /*
    Status codes:
        0 = user successfully added
        -1 = users.txt file not found
        -2 = user already existing
        -3 = IOException
     */
    @Override
    public int insertUser(User u) throws Exception{
        int status = 0;
        boolean found = false;

        if(!isExisting("users")){
            status = -1;
        }else{
            File file = new File(path + "users.txt");
            BufferedReader reader = null;
            FileWriter writer = null;

            try {
                reader = new BufferedReader(new FileReader(file));
                String line = reader.readLine();
                while(!found && line != null){
                    String parts[] = line.split("%&0/\"");
                    if(parts[0].toLowerCase().equals(u.getUsername().toLowerCase())){
                        found=true;
                        status = -2;
                    }
                    line = reader.readLine();
                }
                reader.close();

                if(!found){
                    writer = new FileWriter(file.getPath(), true);
                    writer.write(u.getUsername() + "%&0/\"" + u.getPassword() + "%&0/\"" + u.getEmail() + "%&0/\"" + " " + "%&0/\"\r\n");
                    status = 0;
                    writer.close();
                }

            }catch(Exception e){
                status = -3;
                throw new Exception(e);
            }
        }
        return status;
    }

    @Override
    public User getUser(String username, String pass) {
        User u = new User();
        boolean found = false;

        if(!isExisting("users")){
            return null;
        }else{
            File file = new File(path + "users.txt");
            BufferedReader reader = null;

            try {
                reader = new BufferedReader(new FileReader(file));
                String line = reader.readLine();
                while(!found && line != null){
                    String parts[] = line.split("%&0/\"");
                    if(parts[0].toLowerCase().equals(username.toLowerCase()) && parts[1].equals(pass)){
                        found=true;
                        u.setUsername(username);
                        u.setPassword(pass);
                        u.setEmail(parts[2]);
                    }
                    line = reader.readLine();
                }
                reader.close();

                if(!found){
                    return null;
                }

            }catch(Exception e){
                return null;
            }
        }
        return u;
    }

    @Override
    public String getActiveProfile(String username) {
        boolean found = false;
        String activeProfile = null;

        if(!isExisting("users")){
            return null;
        }else{
            File file = new File(path + "users.txt");
            BufferedReader reader = null;

            try {
                reader = new BufferedReader(new FileReader(file));
                String line = reader.readLine();
                while(!found && line != null){
                    String parts[] = line.split("%&0/\"");
                    if(parts[0].toLowerCase().equals(username.toLowerCase())){
                        activeProfile = parts[3];
                        found = true;
                    }
                    line = reader.readLine();
                }
                reader.close();

                if(!found){
                    return null;
                }

            }catch(Exception e){
                return null;
            }
        }
        return activeProfile;
    }

    @Override
    public void setActiveProfile(String username, String activeProfile) throws IOException {
        boolean found = false;
        String[] text = new String[10000];
        int k = 0;

        if(!isExisting("users")){
            return;
        }else{
            File file = new File(path + "users.txt");
            BufferedReader reader = null;
            FileWriter writer = null;

            try {
                reader = new BufferedReader(new FileReader(file));
                text[k] = reader.readLine();
                while(text[k] != null && k<10000){
                    String parts[] = text[k].split("%&0/\"");
                    if(parts[0].toLowerCase().equals(username.toLowerCase())){
                        text[k] = parts[0] + "%&0/\"" + parts[1] + "%&0/\"" + parts[2] + "%&0/\"" + activeProfile + "%&0/\"";
                        found = true;
                    }
                    k++;
                    text[k] = reader.readLine();
                }
                reader.close();

                if(found) {
                    StringBuilder sb = new StringBuilder();
                    for(int i = 0; i<k; i++){
                        sb.append(text[i] + "\r\n");
                    }

                    writer = new FileWriter(file.getPath(), false);
                    writer.write(sb.toString());
                    writer.close();
                    /*
                    writer = new FileWriter(file.getPath(), true);
                    for (int i = 1; i <= k; i++) {
                        writer.write(text[i] + "\r\n");
                        writer.close();
                    }*/
                }
            }catch(IOException e){
                throw new IOException(e);
            }
        }
    }

    public boolean isExisting(String filename){
        File f = new File(path + filename + ".txt");
        return f.exists();
    }
}
