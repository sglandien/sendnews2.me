package webapp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by fpreuschoff on 24.03.2018.
 */
@WebServlet(name = "changeActiveProfile")
public class changeActiveProfile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = (String)session.getAttribute("username");

        if(username == null){
            response.sendRedirect("index.jsp");
            return;
        }

        String newActiveProfile = request.getParameter("newActiveProfile");

        UserDAO ud = new UserDAOImpl();
        ud.setActiveProfile(username, newActiveProfile);

        request.setAttribute("successMessage","Aktives Profil geändert");
        request.getRequestDispatcher("rss.jsp").forward(request,response);
    }
}
