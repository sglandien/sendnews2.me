package webapp;

import org.w3c.dom.UserDataHandler;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by fpreuschoff on 05.03.2018.
 */
@WebServlet(name = "login")
public class login extends HttpServlet {
    private String host;
    private String port;
    private String user;
    private String pass;

    @Override
    public void init() {
        // reads SMTP server setting from web.xml file
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String submitType = request.getParameter("submit");

        UserDAO ud = new UserDAOImpl();
        User u = null;
        try {
            u = ud.getUser(username, password);
        }catch(Exception e){

        }

        if(submitType.equals("login") && u != null){
            HttpSession session=request.getSession();
            session.setAttribute("username", u.getUsername());
            session.setAttribute("useremail", u.getEmail());
            //request.setAttribute("welcomeMessage", u.getUsername());
            request.getRequestDispatcher("index.jsp").forward(request, response);
            //response.sendRedirect("index.jsp");
        }else if(submitType.equals("register")){
            User newUser = new User();
            newUser.setUsername(username);
            newUser.setEmail(request.getParameter("email"));
            newUser.setPassword(password);
            int status = 10;
            String repeatPassword = request.getParameter("password-repeat");
            if(!password.equals(repeatPassword)) {
                status = -4;
            }else {
                try {
                    status = ud.insertUser(newUser);
                } catch (Exception e) {
                }
            }

            switch(status){
                case 0:
                    Archivierer archiv = new Archivierer();
                    archiv.createArchive(newUser.getUsername());

                    String subject = "Ihre Registrierung bei SendNews2.Me";
                    String content = "Sehr geehrter Kunde,\r\n\r\nSie haben sich bei SendNews2.Me registriert und erhalten somit ab jetzt die neusten Nachrichten, die Sie wuenschen.\r\n\r\n Fangen Sie hier mit dem Einrichten ihres Pressespiegels an: \r\n sendnews2.me/rss.jsp\r\n\r\nVielen Dank!";

                    try {
                        EmailUtility.sendEmail(host, port, user, pass, newUser.getEmail(), subject, content);
                        //EmailUtility.sendEmailWithAttachments(host, port, user, pass, recipient, subject, content, attachFiles);
                    } catch (Exception ex) {
                    }
                    request.setAttribute("successMessage","Successful added user, please login.");
                    break;
                case -1:
                    request.setAttribute("message","No user database found.");
                    break;
                case -2:
                    request.setAttribute("message","User already existing, change username.");
                    break;
                case -3:
                    request.setAttribute("message","Exception reading from user database.");
                    break;
                case -4:
                    request.setAttribute("message", "Passwörter stimmen nicht überein.");
                    break;
                case 10:
                    request.setAttribute("message","No status code received.");
                    break;
                default:
                    request.setAttribute("message","default handler.");
            }
            request.getRequestDispatcher("index.jsp").forward(request,response);
        }else{
            request.setAttribute("message", "Data not found, please register!");
            request.getRequestDispatcher("index.jsp").forward(request,response);
        }
    }
}
