package webapp;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;


public class QueryWithoutContext {

    public static String query(PrintWriter out) throws NamingException {
        MysqlDataSource ds = null;
        Connection connect = null;
        Statement statement = null;
        ArrayList<String> nachricht = new ArrayList<String>();


        try {
            // Create a new DataSource (MySQL specifically)
            // and provide the relevant information to be used by Tomcat.
            ds = new MysqlDataSource();
            ds.setUrl("jdbc:mysql://46.101.234.140:3306/user");
            //ds.setUser("root");
            //ds.setPassword("123");

            ds.setServerName("46.101.234.140");
            ds.setPortNumber(3306);
            ds.setDatabaseName("user");
            ds.setUser("root");
            ds.setPassword("Goldyg123");



            connect = ds.getConnection();

            // Create the statement to be used to get the results.
            statement = connect.createStatement();



            String query = "SELECT * FROM userinformation";

            // Execute the query and get the result set.
            ResultSet resultSet = statement.executeQuery(query);
            out.println("<strong>Printing result using DataSource...</strong><br>");
            while (resultSet.next()) {
                String Vorname = resultSet.getString("Vorname");
                String Nachname = resultSet.getString("Nachname");
                int ID = resultSet.getInt("ID");
                String EMail = resultSet.getString("EMail");

                nachricht.add("Vorname: " + Vorname +
                        ", Nachname: " + Nachname +
                        ", ID-number: " + ID +
                        ", E-Mail Adresse: " + EMail +
                        "<br>");

            }
            String listString = "";

            for (String s : nachricht)
            {
                listString += s + "\t";
            }
        return listString;
        } catch (SQLException e) { e.printStackTrace(out);
        return "No Data";
        } finally {

            // Close the connection and release the resources used.
            try { statement.close(); } catch (SQLException e) { e.printStackTrace(out); }
            try { connect.close(); } catch (SQLException e) { e.printStackTrace(out); }

        }
    }
}
